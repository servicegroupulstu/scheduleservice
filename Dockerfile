FROM openjdk:8
ADD target/demo-0.0.1-SNAPSHOT.jar docker_schedule
EXPOSE 39973
ENTRYPOINT ["java", "-jar", "-Dserver.port=39973", "docker_schedule"]