
create table day (
    id  bigserial,
    group_id int8,
    number_week int4,
    number_day int4,
    primary key (id)
);
create table couple
(
    id           bigserial,
    day_id       int8,
    number_week int4,
    number_day int4,
    info         varchar(255),
    pair_number  int4,
    place        varchar(255),
    subgroup     int4,
    subject      varchar(255),
    teacher      varchar(255),
    name_group    varchar(255),
    type_subject int4,
    primary key (id)
);
create table group_student (
    id  bigserial,
    name_group varchar(255),
    url_group varchar(255),
    primary key (id)
);

alter table if exists couple
    add constraint couple_day_fk
    foreign key (day_id) references day;
alter table if exists day
    add constraint day_group_student_fk
    foreign key (group_id) references group_student;



