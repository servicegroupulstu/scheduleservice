package com.ulstu.scheduleservice.service;



import com.ulstu.scheduleservice.domain.Couple;
import com.ulstu.scheduleservice.domain.Day;
import com.ulstu.scheduleservice.domain.GroupStudent;
import com.ulstu.scheduleservice.repo.CoupleRepo;
import com.ulstu.scheduleservice.repo.DayRepo;
import com.ulstu.scheduleservice.repo.GroupStudentRepo;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class GroupStudentService {
    @Autowired
    private ParserService parserService;

    @Autowired
    private GroupStudentRepo groupStudentRepo;

    @Autowired
    private DayRepo dayRepo;

    @Autowired
    private CoupleRepo coupleRepo;


    public ResponseEntity<?> newGroup(String urlGroup,String nameGroup)  {
        Map<String,String> Response = new HashMap<>();

        //если такая группа уже есть, то кидаем сообщение об этом
        if(groupStudentRepo.existsByNameGroup(nameGroup)){
            Response.put("message","Такая группа уже добавлена");
            return  new ResponseEntity<>(Response,HttpStatus.OK);
        }
        else {
            return  parserService.CreateGroup(urlGroup,nameGroup);
        }
    }

    public ResponseEntity<?> patchGroup(String urlGroup,String nameGroup)  {
        Map<String,String> Response = new HashMap<>();
        //если такой группы не существует, то нет возможности обновить т.к это противречивая операция
        if(groupStudentRepo.existsByNameGroup(nameGroup)){
            return parserService.patchGroup(urlGroup,nameGroup);
        }
        else {
            Response.put("message","Такой групып не сущесвуте, нужно сначала добавить ее");
            return  new ResponseEntity<>("Такой групып не сущесвуте, нужно сначала добавить ее",HttpStatus.BAD_REQUEST);
        }

    }

}
