package com.ulstu.scheduleservice.service;

import com.ulstu.scheduleservice.domain.Couple;
import com.ulstu.scheduleservice.domain.Day;
import com.ulstu.scheduleservice.repo.CoupleRepo;
import com.ulstu.scheduleservice.repo.DayRepo;
import com.ulstu.scheduleservice.repo.GroupStudentRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

@Service
public class TeacherService {
    @Autowired
    private GroupStudentRepo groupStudentRepo;

    @Autowired
    private DayRepo dayRepo;

    @Autowired
    private CoupleRepo coupleRepo;
    public ResponseEntity<?> getListTeacher()  {
        ArrayList<Couple> couples = (ArrayList<Couple>) coupleRepo.findAll();
        Set<String> listTracher = new HashSet<>();
        for (int i = 0; i < couples.size() ; i++) {
            listTracher.add(couples.get(i).getTeacher());
        }
        ArrayList<String> res = new ArrayList<>(listTracher);

        return new ResponseEntity<>(res,HttpStatus.OK);

    }

    public ResponseEntity<?> getScheduleTeacher(String nameTeacher)  {
       try {
           ArrayList<Couple> couples = coupleRepo.findAllByTeacher(nameTeacher);
           ArrayList<Day> days = new ArrayList<>(14);
           for (int i = 0; i < 14; i++) {
               days.add(i,new Day());
           }
           for (Couple s: couples) {
               if(s.getNumberWeek() == 1){
                   Day day = days.get(s.getNumber_day());
                   day.setNumber_day(s.getNumber_day());
                   day.setNumberWeek(s.getNumberWeek());
                   day.getCoupels().add(s);
               }
               else {
                   Day day = days.get(s.getNumber_day()+7);
                   day.setNumber_day(s.getNumber_day());
                   day.setNumberWeek(s.getNumberWeek());
                   day.getCoupels().add(s);
               }
           }
           ArrayList<Day> daysResponse = new ArrayList<>();
           for (int i = 0; i < days.size() ; i++) {
               if(days.get(i).getCoupels().size() != 0){
                   daysResponse.add(days.get(i));
               }
           }
           for (int i = 0; i <daysResponse.size() ; i++) {
               daysResponse.get(i).getCoupels().sort(Couple::compareTo);
           }

           return  new ResponseEntity<>(daysResponse,HttpStatus.OK);

       }catch (Exception e){
           return  new ResponseEntity<>("",HttpStatus.BAD_REQUEST);
       }

    }

}
