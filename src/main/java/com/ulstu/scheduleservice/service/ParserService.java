package com.ulstu.scheduleservice.service;

import com.ulstu.scheduleservice.domain.Couple;
import com.ulstu.scheduleservice.domain.Day;
import com.ulstu.scheduleservice.domain.GroupStudent;
import com.ulstu.scheduleservice.repo.CoupleRepo;
import com.ulstu.scheduleservice.repo.DayRepo;
import com.ulstu.scheduleservice.repo.GroupStudentRepo;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.regex.Pattern;

@Service
public class ParserService {


    @Autowired
    private GroupStudentRepo groupStudentRepo;

    @Autowired
    private DayRepo dayRepo;

    @Autowired
    private CoupleRepo coupleRepo;

    private final Map<String,Integer> WeekStudy = new HashMap<>();
    private final Map<String,Integer> typeStudy = new HashMap<>();
    private final ArrayList<String> listTypeStudu =  new ArrayList<>();


    public  static  String [] ArrDay = {"Пнд","Втр","Срд","Чтв","Птн","Сбт","Вск"};


    public ParserService() {
        WeekStudy.put("Пнд",1);
        WeekStudy.put("Втр",2);
        WeekStudy.put("Срд",3);
        WeekStudy.put("Чтв",4);
        WeekStudy.put("Птн",5);
        WeekStudy.put("Сбт",6);
        WeekStudy.put("Вск",7);
        typeStudy.put("пр",1);
        typeStudy.put("лек",2);
        typeStudy.put("Лаб",3);
        listTypeStudu.add("пр");
        listTypeStudu.add("лек");
        listTypeStudu.add("Лаб");

    }
    public ResponseEntity<?> CreateGroup(String urlGroup,String nameGroup)  {
        try {
            GroupStudent groupStudent = new GroupStudent(nameGroup,urlGroup);
            groupStudentRepo.saveAndFlush(groupStudent);
            return  ParserGroup(nameGroup,0);
        }catch (Exception e){
            return new ResponseEntity<>( "Ошибка в процессе добавления группы",HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public ResponseEntity<?> patchGroup(String urlGroup,String nameGroup )  {
        try {
            GroupStudent groupStudent = groupStudentRepo.findByNameGroup(nameGroup);
            groupStudentRepo.delete(groupStudent);
            GroupStudent groupStudent1 = new GroupStudent(nameGroup,urlGroup);
            groupStudentRepo.save(groupStudent1);
            return  ParserGroup(nameGroup,1);
        }catch (Exception e){
            return new ResponseEntity<>( "Ошибка в процессе обновления группы",HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public Couple ParseCouple(Couple couple,String Subject, String TeacherAndPlace){
        Couple res = new Couple();

        //проверям тип предмета лек,пр, лаб в нек случаях тип не указан, в этмо случае останется дефолтное значение 0
        for (int i = 0; i < listTypeStudu.size(); i++) {
            int check = Subject.indexOf(listTypeStudu.get(i));
            if(check != -1){
                couple.setTypeSubject(typeStudy.get(listTypeStudu.get(i)));
                Subject = Subject.substring(check + listTypeStudu.get(i).length()).trim();
                break;
            }
        }
        String oneGroup = "1 п/г";
        String twoGroup = "2 п/г";
        String threeGroup = "3 п/г";
        String fourGroup = "4 п/г";
        int check_1 = Subject.indexOf(oneGroup);
        int check_2 = Subject.indexOf(twoGroup);
        int check_3 = Subject.indexOf(threeGroup);
        int check_4 = Subject.indexOf(fourGroup);

        if(check_1 != -1){
            Subject = Subject.substring(0,check_1).trim();
            couple.setSubgroup(1);
        }
        else if(check_2 != -1){
            Subject = Subject.substring(0,check_2).trim();
            couple.setSubgroup(2);
        }
        else if(check_3 != -1){
            Subject = Subject.substring(0,check_3).trim();
            couple.setSubgroup(2);
        }
        else if(check_4 != -1){
            Subject = Subject.substring(0,check_4).trim();
            couple.setSubgroup(2);
        }

        couple.setSubject(Subject);
        String teacherExp = "Преподаватели кафeдры";
        int check = TeacherAndPlace.indexOf(teacherExp);
        if(check != -1){
            couple.setTeacher(teacherExp);

            couple.setPlace(TeacherAndPlace.substring(check+1 + teacherExp.length()).trim());
        }
        else {
            for (int i = 0; i < TeacherAndPlace.length() - 6; i++) {
                if (Character.isWhitespace(TeacherAndPlace.charAt(i))
                        && Character.isLetter(TeacherAndPlace.charAt(i + 1))
                        && Character.isWhitespace(TeacherAndPlace.charAt(i + 2))
                        && Character.isLetter(TeacherAndPlace.charAt(i + 3))
                        && Character.isWhitespace(TeacherAndPlace.charAt(i + 4))) {

                    couple.setTeacher(TeacherAndPlace.substring(0,i+5).trim());
                    couple.setPlace(TeacherAndPlace.substring(i+5).trim());
                    break;
                }
            }
        }



        res.setPair_number(couple.getPair_number());
        res.setNumber_day(couple.getNumber_day());
        res.setNumberWeek(couple.getNumberWeek());
        res.setTeacher(couple.getTeacher());
        res.setSubject(couple.getSubject());
        res.setSubgroup(couple.getSubgroup());
        res.setPlace(couple.getPlace());
        res.setTypeSubject(couple.getTypeSubject());
        res.setNameGroup(couple.getNameGroup());
        res.setInfo(couple.getInfo());


        return res;
    }


    //typeHTTP - указывает какой тип операции в зависоми от кода, будет возвращаться разные коды ответов после парсинга группы
    public ResponseEntity<?> ParserGroup(String nameGroup,int typeHTTP)  {
        Map<String,String> Response = new HashMap<>();
        try {
            GroupStudent groupStudent = groupStudentRepo.findByNameGroup(nameGroup);
            Long group_id = groupStudent.getId();
            String urlGroup = groupStudent.getUrl_group();

            //При наличие доступа к станици с расписанием парсим все элементы тега р
            ArrayList<String> htmlInfo = new ArrayList<>();
            ArrayList<Day> Week= new ArrayList<>();

            Document document = Jsoup.connect(urlGroup).get();
            document.outputSettings(new Document.OutputSettings().prettyPrint(false));//makes html() preserve linebreaks and spacing
            document.select("br").append("\\n");
            //document.select("p").prepend("\\n\\n");
            ArrayList<String> A = new ArrayList<>();
            Elements paragraphs = document.getElementsByTag("p");
            String chek;
            for (Element paragraph : paragraphs) {
                chek = paragraph.text();
                htmlInfo.add(chek);
            }
            //создали новую группу и после сохранения получили ее id

            int infoDay = 1;
            for (int i = 0; i < htmlInfo.size(); i++) {
                //если находи след запись, значит дальше будет только вторая неделя, переменная infoDay хранит номер неделе
                if(htmlInfo.get(i).contains("Неделя: 2-я")){
                    infoDay = 2;
                    continue;
                }
                //если находим названия дня неделе по словарю, то значит след 8 строк содержат в себе пары, но допустимо что строка может быть пустой
                //есть переменная flag если хотя есть одна пара в текущей день, то значит этот день нужно сохранить
                if(WeekStudy.containsKey(htmlInfo.get(i))){
                    ArrayList<Couple> couples = new ArrayList<>(20);
                    Day day = new Day();
                    day.setNumberWeek(infoDay);
                    day.setNumber_day(WeekStudy.get(htmlInfo.get(i)));
                    day.setGroup_id(group_id);
                    dayRepo.save(day);
                    Long dayId = day.getId();

                    //если в данный день нет предметов, то он не сохраняется
                    boolean flag = false;
                    for (int k = 1; k <= 8 ; k++) {
                        if(htmlInfo.get(i+k).equals(""))
                            continue;
                        Couple couple = new Couple();
                        couple.setDay_id(dayId);
                        couple.setInfo(htmlInfo.get(i+k));
                        couple.setPair_number(k);
                        couple.setSubgroup(0);
                        couple.setTypeSubject(0);
                        couple.setNumber_day(k);
                        couple.setNumberWeek(infoDay);
                        couple.setNameGroup(groupStudent.getNameGroup());
                        flag = true;
                        //Начинаем парсить строку чтобы извлечь данные о текущей паре, сначала удаляем все ненужные символы
                        String ParseSubject = htmlInfo.get(i+k).trim().replace('.',' ');
                        ParseSubject = ParseSubject.replace('-',' ');

                        //раздаелям строку по символу переноса строк и в зависимости от размера полученного массива будем обрабатывать по разному
                        String [] ArrStr = ParseSubject.split(Pattern.quote("\\n"));

                        if(ArrStr.length == 2){
                            Couple res = ParseCouple(couple,ArrStr[0],ArrStr[1]);
                            coupleRepo.save(res);
                            couples.add(res);
                        }
                        else if(ArrStr.length == 3){
                            Couple res1 = ParseCouple(couple,ArrStr[0],ArrStr[1]);
                            Couple res2 = ParseCouple(couple,ArrStr[0],ArrStr[2]);
                            coupleRepo.save(res1);
                            coupleRepo.save(res2);
                            couples.add(res1);
                            couples.add(res2);
                        }
                        else if(ArrStr.length == 4){
                            Couple res1 = ParseCouple(couple,ArrStr[0],ArrStr[1]);
                            Couple res2 = ParseCouple(couple,ArrStr[2],ArrStr[3]);
                            couples.add(res2);
                            couples.add(res1);
                            coupleRepo.save(res1);
                            coupleRepo.save(res2);

                        }

                    }

                    if(flag){
                        day.setCoupels(couples);
                        dayRepo.save(day);
                        Week.add(day);
                    }
                    else {
                        dayRepo.delete(day);
                    }

                }

            }
            groupStudent.setDays(Week);
            groupStudentRepo.save(groupStudent);
            switch (typeHTTP){
                case 0:
                    Response.put("message","Группа добавлена");
                    return new ResponseEntity<>(Response,HttpStatus.CREATED);
                case 1:
                    Response.put("message","Группа обновлена");
                    return new ResponseEntity<>(Response,HttpStatus.OK);
                default:
                    Response.put("message","Неизвестный тип операции");
                    return new ResponseEntity<>(Response,HttpStatus.BAD_REQUEST);
            }

        }catch (Exception e){
            Response.put("message","Ошибка в процессе парсинга группы");
            return new ResponseEntity<>(Response,HttpStatus.INTERNAL_SERVER_ERROR);
        }


    }
}
