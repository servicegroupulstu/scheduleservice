package com.ulstu.scheduleservice.repo;

import com.ulstu.scheduleservice.domain.GroupStudent;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.ArrayList;
import java.util.Optional;

public interface GroupStudentRepo  extends JpaRepository<GroupStudent,Long> {
    GroupStudent findByNameGroup(String s);
    boolean existsByNameGroup(String s);
}
