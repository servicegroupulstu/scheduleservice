package com.ulstu.scheduleservice.repo;

import com.ulstu.scheduleservice.domain.Day;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface DayRepo extends JpaRepository<Day,Long> {

}
