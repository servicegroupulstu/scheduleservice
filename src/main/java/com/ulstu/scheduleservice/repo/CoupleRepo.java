package com.ulstu.scheduleservice.repo;

import com.ulstu.scheduleservice.domain.Couple;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.ArrayList;


public interface CoupleRepo extends JpaRepository<Couple,Long> {
    ArrayList<Couple> findAllByTeacher(String nameTeacher);

}
