package com.ulstu.scheduleservice.controller;

import com.ulstu.scheduleservice.domain.Couple;
import com.ulstu.scheduleservice.domain.Day;
import com.ulstu.scheduleservice.domain.GroupStudent;
import com.ulstu.scheduleservice.repo.CoupleRepo;
import com.ulstu.scheduleservice.repo.DayRepo;
import com.ulstu.scheduleservice.repo.GroupStudentRepo;
import com.ulstu.scheduleservice.service.GroupStudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Query;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
public class GroupController {
    @Autowired
    private CoupleRepo coupleRepo;
    @Autowired
    private DayRepo dayRepo;
    @Autowired
    private GroupStudentRepo groupStudentRepo;

    @Autowired
    private GroupStudentService groupStudentService;


    @GetMapping("/hello")
    public ResponseEntity<?> hi(){
        return  new ResponseEntity<>("Hello world",HttpStatus.OK);
    }


    @PostMapping("/group-list")
    public ResponseEntity<?> getListGroups(){
        ArrayList<GroupStudent> groupStudents = (ArrayList<GroupStudent>) groupStudentRepo.findAll();
        ArrayList<String> listNameGroups = new ArrayList<>(groupStudents.size());
        for (int i = 0; i < groupStudents.size() ; i++) {
            listNameGroups.add(groupStudents.get(i).getNameGroup());
        }
        return  new ResponseEntity<>(listNameGroups,HttpStatus.OK);
    }

    @PostMapping("/group")
    public ResponseEntity<?> createGroup(@RequestBody GroupStudent groupStudent){
       return groupStudentService.newGroup(groupStudent.getUrl_group(),groupStudent.getNameGroup());
    }
    @GetMapping("/group")
    public ResponseEntity<?> getGroup(@RequestParam("nameGroup") String nameGroup){
        GroupStudent groupStudent = groupStudentRepo.findByNameGroup(nameGroup);
        return  new ResponseEntity<>(groupStudent,HttpStatus.OK);

    }
    @PatchMapping("/group")
    public ResponseEntity<?> patchGroup(@RequestBody GroupStudent groupStudent){
        return groupStudentService.patchGroup(groupStudent.getUrl_group(),groupStudent.getNameGroup());

    }




}
