package com.ulstu.scheduleservice.controller;


import com.ulstu.scheduleservice.domain.Couple;
import com.ulstu.scheduleservice.domain.GroupStudent;
import com.ulstu.scheduleservice.repo.CoupleRepo;
import com.ulstu.scheduleservice.repo.DayRepo;
import com.ulstu.scheduleservice.repo.GroupStudentRepo;
import com.ulstu.scheduleservice.service.GroupStudentService;
import com.ulstu.scheduleservice.service.TeacherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;

@RestController
public class TeacherController {
    @Autowired
    private CoupleRepo coupleRepo;
    @Autowired
    private DayRepo dayRepo;
    @Autowired
    private GroupStudentRepo groupStudentRepo;

    @Autowired
    private GroupStudentService groupStudentService;
    @Autowired
    private TeacherService teacherService;

    @GetMapping("/teacher")
    public ResponseEntity<?> getTeacher(@RequestParam("nameTeacher") String nameTeacher){
        return teacherService.getScheduleTeacher(nameTeacher);
    }

    @PostMapping("/teacher-list")
    public ResponseEntity<?> getTeacherList(){
        return teacherService.getListTeacher();
    }
}
