package com.ulstu.scheduleservice.domain;


import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Day {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Integer number_day;
    private Long group_id;
    private Integer numberWeek;
    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "day_id", referencedColumnName = "id")
    private List<Couple> coupels = new ArrayList<>();


    public Day() {
    }

    public Day(Long id, Integer number_day, Long group_id, Integer numberWeek, List<Couple> coupels) {
        this.id = id;
        this.number_day = number_day;
        this.group_id = group_id;
        this.numberWeek = numberWeek;
        this.coupels = coupels;
    }

    public Integer getNumberWeek() {
        return numberWeek;
    }

    public void setNumberWeek(Integer numberWeek) {
        this.numberWeek = numberWeek;
    }

    public Integer getNumber_day() {
        return number_day;
    }

    public void setNumber_day(Integer number_day) {
        this.number_day = number_day;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getGroup_id() {
        return group_id;
    }

    public void setGroup_id(Long group_id) {
        this.group_id = group_id;
    }

    public List<Couple> getCoupels() {
        return coupels;
    }

    public void setCoupels(List<Couple> coupels) {
        this.coupels = coupels;
    }

    @Override
    public String toString() {
        return "Day{" +
                "id=" + id +
                ", number_day=" + number_day +
                ", group_id=" + group_id +
                ", coupels=" + coupels +
                '}';
    }
}
