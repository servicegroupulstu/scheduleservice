package com.ulstu.scheduleservice.domain;

import org.hibernate.annotations.JoinColumnOrFormula;
import org.hibernate.annotations.JoinFormula;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Entity
@Table(name = "group_student")
public class GroupStudent {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    //@SequenceGenerator(name="identifier", sequenceName="id", allocationSize=1)
    //@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="identifier")
    private Long id;

    private String nameGroup;
    private String url_group;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "group_id", referencedColumnName = "id")
    private List<Day> Days = new ArrayList<>();



    public GroupStudent() {
    }

    public GroupStudent(Long id) {
        this.id = id;
    }

    public GroupStudent(String nameGroup) {
        this.nameGroup = nameGroup;
    }



    public GroupStudent(String nameGroup, String url_group) {
        this.nameGroup = nameGroup;
        this.url_group = url_group;
    }

    public List<Day> getDays() {
        return Days;
    }

    public void setDays(List<Day> days) {
        Days = days;
    }

    public String getNameGroup() {
        return nameGroup;
    }

    public void setNameGroup(String nameGroup) {
        this.nameGroup = nameGroup;
    }




    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }



    public String getUrl_group() {
        return url_group;
    }

    public void setUrl_group(String url_group) {
        this.url_group = url_group;
    }

}
