package com.ulstu.scheduleservice.domain;

import javax.persistence.*;

@Entity
@Table(name = "couple")
public class Couple implements Comparable<Couple>{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Long day_id;
    private Integer pair_number;
    private Integer number_day;
    private Integer numberWeek;
    private String teacher;
    private String subject;
    private Integer subgroup;
    private String place;
    private Integer typeSubject;
    private String nameGroup;
    private String info;

    public Couple() {
    }

    public Couple(Long id, Long day_id, Integer pair_number, Integer number_day,
                  Integer numberWeek, String teacher, String subject, Integer subgroup,
                  String place, Integer typeSubject, String nameGroup, String info) {
        this.id = id;
        this.day_id = day_id;
        this.pair_number = pair_number;
        this.number_day = number_day;
        this.numberWeek = numberWeek;
        this.teacher = teacher;
        this.subject = subject;
        this.subgroup = subgroup;
        this.place = place;
        this.typeSubject = typeSubject;
        this.nameGroup = nameGroup;
        this.info = info;
    }

    public Integer getNumber_day() {
        return number_day;
    }

    public void setNumber_day(Integer number_day) {
        this.number_day = number_day;
    }

    public Integer getNumberWeek() {
        return numberWeek;
    }

    public void setNumberWeek(Integer numberWeek) {
        this.numberWeek = numberWeek;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public Integer getPair_number() {
        return pair_number;
    }

    public void setPair_number(Integer pair_number) {
        this.pair_number = pair_number;
    }

    public Long getDay_id() {
        return day_id;
    }

    public void setDay_id(Long day_id) {
        this.day_id = day_id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTeacher() {
        return teacher;
    }

    public void setTeacher(String teacher) {
        this.teacher = teacher;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public Integer getSubgroup() {
        return subgroup;
    }

    public String getNameGroup() {
        return nameGroup;
    }

    public void setNameGroup(String nameGroup) {
        this.nameGroup = nameGroup;
    }

    public void setSubgroup(Integer subgroup) {
        this.subgroup = subgroup;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public Integer getTypeSubject() {
        return typeSubject;
    }

    public void setTypeSubject(Integer typeSubject) {
        this.typeSubject = typeSubject;
    }

    @Override
    public int compareTo(Couple o) {
        int compareQuantity = ((Couple) o).getPair_number();

        //ascending order
        return this.getPair_number() - compareQuantity;

        //descending order
        //return compareQuantity - this.quantity;
    }
}
